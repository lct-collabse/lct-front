import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TemplateComponent } from '@layouts/template';
import { GlobalRouting, UtilsRouting } from '@core/enums';


const routes: Routes = [
  {
    path: GlobalRouting.AUTH,
    loadChildren: () => import('./auth').then(m => m.AuthModule),
  },
  {
    path: UtilsRouting.EMPTY,
    component: TemplateComponent,
    loadChildren: () => import('@modules/modules.module').then(m => m.ModulesModule)
  },
  { path: UtilsRouting.ANY, redirectTo: UtilsRouting.EMPTY },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
