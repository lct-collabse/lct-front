import { ChangeDetectionStrategy, Component, Inject, OnInit, Self } from '@angular/core';
import { FormControl } from '@angular/forms';

// STATE
import { StateService } from '@core/services';

// EXTRA
import { AppMode, GlobalRouting, UserRole } from '@core/enums';
import { ROLES_LIST } from '@core/consts';
import { ListOption } from '@core/interfaces';
import { TuiDestroyService } from '@taiga-ui/cdk';
import { takeUntil } from 'rxjs';
import { AuthUserDataDto } from '@core/api/models';


interface INavigation {
  label: string;
  path: string;
}

enum UserAction {
  OPEN_PROFILE = 'OPEN_PROFILE',
  LOG_OUT = 'OPEN_PROFILE',
}


const NAV_LINKS: INavigation[] = [
  // { label: 'Главная',       path: GlobalRouting.MAIN },
];

const USER_ACTIONS: ListOption<UserAction>[] = [
  {
    code: UserAction.OPEN_PROFILE,
    title: 'Профиль',
  },
  {
    code: UserAction.LOG_OUT,
    title: 'Выйти',
  },
];

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TuiDestroyService],
})
export class HeaderComponent implements OnInit {
  readonly AppMode = AppMode;

  public roles: ListOption<UserRole>[] = ROLES_LIST;
  public demoUserRoleControl = new FormControl<ListOption<UserRole> | null>(null);

  public navLinks: INavigation[] = NAV_LINKS;
  public userActions: ListOption<UserAction>[] = USER_ACTIONS;

  public user: AuthUserDataDto | null = null;
  public rolePreview: string | null = null;

  public isMenuOpened: boolean = false;
  public appMode: AppMode | null = null;

  constructor(
    private readonly state: StateService,

    @Self()
    @Inject(TuiDestroyService)
    private readonly destroy$: TuiDestroyService,
  ) {}

  ngOnInit(): void {
    this.appMode = this.state.appMode$.getValue()!;
    this.setupRoleHandling();
  }

  public handleAction(code: UserAction): void {
    if (code === UserAction.LOG_OUT) {
      this.state.logout();
    }
  }

  private setupRoleHandling(): void {
    if (this.appMode === AppMode.DEMO) {
      const demoUserRoleCode: UserRole = this.state.demoUserRole$.getValue()!;

      const demoUserRole: ListOption<UserRole> = ROLES_LIST
        .find((item: ListOption<UserRole>) => item.code === demoUserRoleCode)!;
      this.demoUserRoleControl.setValue(demoUserRole);

      this.demoUserRoleControl.valueChanges.subscribe((newDemoUserRole: any) => {
        const newDemoUserRoleCode: UserRole = (newDemoUserRole! as ListOption<UserRole>).code;
        this.state.changeDemoUserRole(newDemoUserRoleCode);
      });

      return;
    }

    this.state.user$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: AuthUserDataDto | null)  => {
        this.user = data;

        this.rolePreview = ROLES_LIST
          .find(item => item.code === this.user?.role)
          ?.title as string;
      });
  }
}
