import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import {
  TuiButtonModule,
  TuiDataListModule,
  TuiDropdownModule,
  TuiHostedDropdownModule,
  TuiLinkModule,
  TuiSvgModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import {
  TuiDataListWrapperModule,
  TuiSelectModule,
  TuiTabsModule,
} from '@taiga-ui/kit';

import { TemplateComponent } from './template';
import { HeaderComponent } from './header';
import { FooterComponent } from './footer';


const BASE_MODULES = [
  CommonModule,
  RouterModule,
  ReactiveFormsModule,
];

const TAIGA_MODULES = [
  TuiButtonModule,
  TuiDataListModule,
  TuiDataListWrapperModule,
  TuiDropdownModule,
  TuiHostedDropdownModule,
  TuiLinkModule,
  TuiSelectModule,
  TuiSvgModule,
  TuiTabsModule,
  TuiTextfieldControllerModule,
];

const COMPONENTS = [
  HeaderComponent,
  FooterComponent,
];

@NgModule({
  imports: [
    ...BASE_MODULES,
    ...TAIGA_MODULES,
  ],
  declarations: [
    TemplateComponent,
    ...COMPONENTS,
  ],
})
export class LayoutModule {}
