import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateComponent {}
