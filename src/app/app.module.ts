import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { TuiRootModule } from "@taiga-ui/core";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LayoutModule } from '@layouts/layoyuts.module';
import { CoreModule } from '@core/api/core.module';

// import { NgDompurifySanitizer } from "@tinkoff/ng-dompurify";
// import { TuiRootModule, TuiDialogModule, TuiAlertModule, TUI_SANITIZER } from "@taiga-ui/core";
// import {TuiAppBarModule} from '@taiga-ui/addon-mobile';
// import {TuiDataListModule} from '@taiga-ui/core';
// import {TuiSvgModule} from '@taiga-ui/core';
// import {TuiHostedDropdownModule} from '@taiga-ui/core';
// import {TuiTabsModule} from '@taiga-ui/kit';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    TuiRootModule,

    CoreModule,
    LayoutModule,
    AppRoutingModule,

    // TuiDialogModule,
    // TuiAlertModule,
    // TuiAppBarModule,
    // TuiDataListModule,
    // TuiSvgModule,
    // TuiHostedDropdownModule,
    // TuiTabsModule,
],
  providers: [
    // {provide: TUI_SANITIZER, useClass: NgDompurifySanitizer},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
