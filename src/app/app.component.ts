import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { StateService } from '@core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  constructor(private readonly state: StateService) {}

  ngOnInit(): void {
    this.state.init();
  }
}
