import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UtilsRouting } from '@core/enums';
import { AuthRouting } from './auth-routing.enum';

import { AuthTemplateComponent } from './layouts';
import { LoginComponent, SignupComponent } from './pages';


const routes: Routes = [
  {
    path: UtilsRouting.EMPTY,
    component: AuthTemplateComponent,
    children: [
      { path: AuthRouting.LOGIN, component: LoginComponent },
      { path: AuthRouting.SIGNUP, component: SignupComponent },

      { path: UtilsRouting.ANY, redirectTo: AuthRouting.LOGIN },
    ],
  },

  { path: UtilsRouting.ANY, redirectTo: UtilsRouting.EMPTY },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
