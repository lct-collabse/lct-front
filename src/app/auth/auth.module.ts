import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
  TuiButtonModule,
  TuiDataListModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';
import {
  TuiDataListWrapperModule,
  TuiInputModule,
  TuiInputPasswordModule,
  TuiSelectModule,
} from '@taiga-ui/kit';

import { AuthRoutingModule } from './auth-routing.module';

import { AuthTemplateComponent } from './layouts';
import { LoginComponent, SignupComponent } from './pages';


const BASE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
];

const TAIGA_MODULES = [
  TuiButtonModule,
  TuiDataListModule,
  TuiDataListWrapperModule,
  TuiInputModule,
  TuiInputPasswordModule,
  TuiSelectModule,
  TuiTextfieldControllerModule,
];

const PAGES = [
  LoginComponent,
  SignupComponent,
];

@NgModule({
  imports: [
    ...BASE_MODULES,
    ...TAIGA_MODULES,
    AuthRoutingModule,
  ],
  declarations: [
    AuthTemplateComponent,
    ...PAGES,
  ],
})
export class AuthModule { }
