import { ChangeDetectionStrategy, Component, Inject, Self } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { concatMap, takeUntil } from 'rxjs';
import { TuiDestroyService } from '@taiga-ui/cdk';

// API MODELS
import { AuthLoginResponseDto, AuthSignupResponseDto, AuthUserDataDto } from '@core/api/models';

// API SERVICES
import { AuthService } from '@core/api/services';

// STATE
import { StateService } from '@core/services';

// EXTRA
import { AuthRouting } from '@auth/index';
import { ROLES_LIST } from '@core/consts';
import { GlobalRouting, UserRole } from '@core/enums';
import { ListOption } from '@core/interfaces';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TuiDestroyService],
})
export class SignupComponent {
  public roles: ListOption<UserRole>[] = ROLES_LIST;

  public form = new FormGroup({
    role: new FormControl(null, Validators.required),
    login: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });

  constructor(
    private readonly router: Router,
    private readonly state: StateService,
    private readonly authService: AuthService,

    @Self()
    @Inject(TuiDestroyService)
    private readonly destroy$: TuiDestroyService,
  ) {}

  public onSubmit(): void {
    const { role, login, password } = this.form.value!;
    const roleCode: UserRole = (role! as ListOption<UserRole>).code;

    this.authService
      .signup(roleCode, login!, password!)
      .pipe(
        concatMap((result: AuthSignupResponseDto) => {
          return this.authService.login(login!, password!);
        }),
        takeUntil(this.destroy$),
      )
      .subscribe((data: AuthLoginResponseDto) => {
        this.state.login(data.user as AuthUserDataDto, data.token);
      });
  }

  public goToLogin(): void {
    this.router.navigate([`/${GlobalRouting.AUTH}/${AuthRouting.LOGIN}`]);
  }
}
