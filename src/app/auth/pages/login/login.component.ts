import { ChangeDetectionStrategy, Component, Inject, Self } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs';
import { TuiDestroyService } from '@taiga-ui/cdk';

// API MODELS
import { AuthLoginResponseDto, AuthUserDataDto } from '@core/api/models';

// API SERVICES
import { AuthService } from '@core/api/services';

// STATE
import { StateService } from '@core/services';

// EXTRA
import { AuthRouting } from '@auth/index';
import { GlobalRouting } from '@core/enums';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TuiDestroyService],
})
export class LoginComponent {
  public form = new FormGroup({
    login: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private readonly router: Router,
    private readonly state: StateService,
    private readonly authService: AuthService,

    @Self()
    @Inject(TuiDestroyService)
    private readonly destroy$: TuiDestroyService,
  ) {}

  public onSubmit(): void {
    const { login, password } = this.form.value!;

    this.authService
      .login(login!, password!)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: AuthLoginResponseDto) => {
        this.state.login(data.user as AuthUserDataDto, data.token);
      });
  }

  public goToSignup(): void {
    this.router.navigate([`/${GlobalRouting.AUTH}/${AuthRouting.SIGNUP}`]);
  }
}
