export function cutList<T>(items: T[]): T[] {
  return items.filter((item, index) => index % 2 !== 0);
}
