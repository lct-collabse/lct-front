import { Pipe, PipeTransform } from '@angular/core';
import { NameFormat } from '@core/enums';
import { BaseUserModel } from '@core/models';


@Pipe({
  name: 'formatName'
})
export class FormatNamePipe implements PipeTransform {

  transform(user: BaseUserModel, mode: NameFormat): string {
    const { lastName, firstName, middleName } = user;
    let result = '';

    switch (mode) {
      case NameFormat.CASUAL:
        result = `${firstName} ${lastName}`;
        break;

      case NameFormat.SHORT:
        result = `${lastName} ${firstName?.charAt(0)}.${middleName?.charAt(0)}.`;
        break;

      case NameFormat.LONG:
        result = `${lastName} ${firstName} ${middleName}`;
        break;
    }

    return result;
  }

}
