import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatNamePipe } from './format-name.pipe';


@NgModule({
  imports: [CommonModule],
  declarations: [FormatNamePipe],
  exports: [FormatNamePipe]
})
export class FormatNamePipeModule { }
