import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GlobalRouting, UtilsRouting } from '@core/enums';


const routes: Routes = [
  {
    path: UtilsRouting.EMPTY,
    loadChildren: () => import('./main').then(m => m.MainModule),
  },

  { path: UtilsRouting.ANY, redirectTo: UtilsRouting.EMPTY },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
