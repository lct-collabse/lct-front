import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StudentPosition, UserRole, UtilsRouting } from '@core/enums';

import {
  CuratorComponent,
  HrComponent,
  MentorComponent,

  StudentComponent,
  CandidateComponent,
  InternComponent,
} from './pages';


const routes: Routes = [
  { path: UserRole.CURATOR, component: CuratorComponent },
  { path: UserRole.HR, component: HrComponent },
  { path: UserRole.MENTOR, component: MentorComponent },
  { path: UserRole.STUDENT, component: StudentComponent, children: [
      { path: StudentPosition.CANDIDATE, component: CandidateComponent },
      { path: StudentPosition.INTERN, component: InternComponent },

      { path: UtilsRouting.ANY, redirectTo: StudentPosition.CANDIDATE },
  ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
