import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  TuiButtonModule,
  TuiDataListModule,
  TuiScrollbarModule,
  TuiSvgModule,
  TuiTextfieldControllerModule,
} from '@taiga-ui/core';

import {
  TuiAvatarModule,
  TuiDataListWrapperModule,
  TuiSelectModule,
} from '@taiga-ui/kit';

import {
  TuiArcChartModule,
  TuiAxesModule,
  TuiLegendItemModule,
  TuiLineChartModule,
  TuiRingChartModule,
} from '@taiga-ui/addon-charts';

import { FormatNamePipeModule } from '@shared/pipes';

import { MainRoutingModule } from './main-routing.module';

import {
  CandidateComponent,
  CuratorComponent,
  HrComponent,
  InternComponent,
  MentorComponent,
  StudentComponent,
} from './pages';

import {
  EventsListComponent,
  GrowthPathComponent,
  PreviewCardComponent,
  RequestsStatisticsComponent,
  SchoolRatingComponent,
  StudyTimesheetComponent,
  UserActionsComponent,
  UsersListComponent,
  UsersRatingComponent,
  UsersStatisticsComponent,
} from './components';


const BASE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
];

const TAIGA_MODULES = [
  TuiArcChartModule,
  TuiAvatarModule,
  TuiAxesModule,
  TuiButtonModule,
  TuiDataListModule,
  TuiDataListWrapperModule,
  TuiLegendItemModule,
  TuiLineChartModule,
  TuiRingChartModule,
  TuiScrollbarModule,
  TuiSelectModule,
  TuiSvgModule,
  TuiTextfieldControllerModule,
];

const CUSTOM_MODULES = [
  FormatNamePipeModule,
];

const PAGES = [
  CuratorComponent,
  HrComponent,
  MentorComponent,
  StudentComponent,
  CandidateComponent,
  InternComponent,
];

const COMPONENTS = [
  PreviewCardComponent,

  UserActionsComponent,
  UsersListComponent,
  UsersRatingComponent,
  UsersStatisticsComponent,

  GrowthPathComponent,
  EventsListComponent,
  StudyTimesheetComponent,

  SchoolRatingComponent,
  RequestsStatisticsComponent,
];

@NgModule({
  imports: [
    ...BASE_MODULES,
    ...TAIGA_MODULES,
    ...CUSTOM_MODULES,
    MainRoutingModule,
  ],
  declarations: [
    ...PAGES,
    ...COMPONENTS,
  ],
})
export class MainModule { }
