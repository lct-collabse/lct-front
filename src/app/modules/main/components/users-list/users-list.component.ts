import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { NameFormat } from '@core/enums';
import { BaseUserModel } from '@core/models';


@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersListComponent {
  @Input() public format!: NameFormat;

  @Input() public users: BaseUserModel[] = [];

  @Input() public btnText?: string;
}
