import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { NameFormat } from '@core/enums';
import { BaseUserModel, RatingModel } from '@core/models';

import { MOCK_INTERNS_RATING } from '@core/mocks';


const KEY_POSITION = 41;
const RATING_TOTAL = 100;

@Component({
  selector: 'app-users-rating',
  templateUrl: './users-rating.component.html',
  styleUrls: ['./users-rating.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersRatingComponent {
  public format = NameFormat.CASUAL;

  public keyPosition = KEY_POSITION;
  public ratingTotal = RATING_TOTAL;

  public ratingList: RatingModel<BaseUserModel>[] = MOCK_INTERNS_RATING;
}
