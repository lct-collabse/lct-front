import { ChangeDetectionStrategy, Component } from '@angular/core';
import { StatisticsModel } from '@core/models';
import { tuiSum } from '@taiga-ui/cdk';


const REQUESTS_STATISTICS: StatisticsModel[] = [
  { label: 'IT',                            value: 16 },
  { label: 'HR-город',                      value: 7 },
  { label: 'Городская экономика',           value: 12 },
  { label: 'Медийный город',                value: 19 },
  { label: 'Социальный город',              value: 8 },
  { label: 'Комфортная и городская среда',  value: 14 },
  { label: 'Правовое пространство',         value: 11 },
];

@Component({
  selector: 'app-requests-statistics',
  templateUrl: './requests-statistics.component.html',
  styleUrls: ['./requests-statistics.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestsStatisticsComponent {
  readonly labels: string[] = REQUESTS_STATISTICS.map(({ label }) => label);
  readonly values: number[] = REQUESTS_STATISTICS.map(({ value }) => value);

  public total = tuiSum(...this.values);
  public activeItemIndex = NaN;

  public onHover(index: number, hovered: any): void {
    this.activeItemIndex = hovered ? index : 0;
  }

  public isItemActive(index: number): boolean {
    return this.activeItemIndex === index;
  }

  public getColor(index: number): string {
    return `var(--tui-chart-${index})`;
  }
}
