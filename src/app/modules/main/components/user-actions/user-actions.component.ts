import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { ListOption } from '@core/interfaces';


@Component({
  selector: 'app-user-actions',
  templateUrl: './user-actions.component.html',
  styleUrls: ['./user-actions.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserActionsComponent {
  @Input() public actions: ListOption[] = [];

  @Output() public selected = new EventEmitter();

  public handleAction(code: string): void {
    this.selected.emit(code);
  }
}
