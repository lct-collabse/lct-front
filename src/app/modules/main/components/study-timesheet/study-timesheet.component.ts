import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-study-timesheet',
  templateUrl: './study-timesheet.component.html',
  styleUrls: ['./study-timesheet.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudyTimesheetComponent {

}
