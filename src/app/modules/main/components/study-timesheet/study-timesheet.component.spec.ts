import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyTimesheetComponent } from './study-timesheet.component';

describe('StudyTimesheetComponent', () => {
  let component: StudyTimesheetComponent;
  let fixture: ComponentFixture<StudyTimesheetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StudyTimesheetComponent]
    });
    fixture = TestBed.createComponent(StudyTimesheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
