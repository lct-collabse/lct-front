import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GrowthPathComponent } from './growth-path.component';

describe('GrowthPathComponent', () => {
  let component: GrowthPathComponent;
  let fixture: ComponentFixture<GrowthPathComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GrowthPathComponent]
    });
    fixture = TestBed.createComponent(GrowthPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
