import { ChangeDetectionStrategy, Component } from '@angular/core';


const JOBS_STATISTICS: number[] = [45, 62, 57, 39, 73];


const SKILLS_STATISTICS: number[] = [88, 26, 91, 35, 80];

@Component({
  selector: 'app-growth-path',
  templateUrl: './growth-path.component.html',
  styleUrls: ['./growth-path.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GrowthPathComponent {
  readonly jobsStatistics: number[] = JOBS_STATISTICS;
  readonly skillsStatistics: number[] = SKILLS_STATISTICS;

  public jobsActiveIndex = NaN;
  public skillsActiveIndex = NaN;
}
