import { ChangeDetectionStrategy, Component } from '@angular/core';

import { BaseUserModel } from '@core/models';
import { NameFormat } from '@core/enums';

import { MOCK_CANDIDATES_LIST, MOCK_MENTORS_LIST } from '@core/mocks';
import { cutList } from '@shared/utils';


enum SchoolMode {
  CAREER_SCHOOL = 'career-school',
  MENTOR_SCHOOL = 'mentor-school',
}
enum MoveDirection {
  NEXT = 'next',
  PREV = 'prev',
}

const MODES_LIST: SchoolMode[] = [
  SchoolMode.CAREER_SCHOOL,
  SchoolMode.MENTOR_SCHOOL,
];
const MODE_TITLES: Record<SchoolMode, string> = {
  [SchoolMode.CAREER_SCHOOL]: 'Карьерная школа',
  [SchoolMode.MENTOR_SCHOOL]: 'Школа наставников',
};

@Component({
  selector: 'app-school-rating',
  templateUrl: './school-rating.component.html',
  styleUrls: ['./school-rating.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchoolRatingComponent {
  readonly NameFormat = NameFormat;
  readonly SchoolMode = SchoolMode;
  readonly MoveDirection = MoveDirection;

  public approvedCandidates: BaseUserModel[] = cutList<BaseUserModel>(MOCK_CANDIDATES_LIST);
  public approvedMentors: BaseUserModel[] = cutList<BaseUserModel>(MOCK_MENTORS_LIST);

  public modeIndex: number = 0;
  public currentMode: SchoolMode = MODES_LIST[this.modeIndex];
  public modeTitles: Record<SchoolMode, string> = MODE_TITLES;

  public handleMove(direction: MoveDirection): void {
    switch (direction) {
      case MoveDirection.NEXT:
        this.modeIndex = (this.modeIndex + 1) % MODES_LIST.length;
        break;

      case MoveDirection.PREV:
        this.modeIndex = Math.abs(this.modeIndex - 1) % MODES_LIST.length;
        break;
    }

    this.currentMode = MODES_LIST[this.modeIndex];
  }
}
