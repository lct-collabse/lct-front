import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolRatingComponent } from './school-rating.component';

describe('SchoolRatingComponent', () => {
  let component: SchoolRatingComponent;
  let fixture: ComponentFixture<SchoolRatingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SchoolRatingComponent]
    });
    fixture = TestBed.createComponent(SchoolRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
