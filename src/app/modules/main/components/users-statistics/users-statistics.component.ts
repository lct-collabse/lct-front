import { ChangeDetectionStrategy, Component } from '@angular/core';
import { tuiSum } from '@taiga-ui/cdk';


interface IStatistic {
  label: string;
  value: number;
}

const USERS_STATISTICS: IStatistic[] = [
  { label: 'Кураторы',  value: 3 },
  { label: 'Наставники',   value: 10 },
  { label: 'Кадровики', value: 5 },
  { label: 'Студенты',  value: 28 },
];

@Component({
  selector: 'app-users-statistics',
  templateUrl: './users-statistics.component.html',
  styleUrls: ['./users-statistics.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersStatisticsComponent {
  readonly labels: string[] = USERS_STATISTICS.map(({ label }) => label);
  readonly values: number[] = USERS_STATISTICS.map(({ value }) => value);

  public total = tuiSum(...this.values);
  public activeItemIndex = NaN;

  public onHover(index: number, hovered: any): void {
    this.activeItemIndex = hovered ? index : 0;
  }

  public isItemActive(index: number): boolean {
    return this.activeItemIndex === index;
  }

  public getColor(index: number): string {
    return `var(--tui-chart-${index})`;
  }
}
