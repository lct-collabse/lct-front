import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EventsListComponent {

}
