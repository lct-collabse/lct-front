import { ChangeDetectionStrategy, Component } from '@angular/core';

import { NameFormat } from '@core/enums';
import { ListOption } from '@core/interfaces';
import { BaseUserModel, OrganizationModel } from '@core/models';

import { MOCK_CANDIDATES_LIST, MOCK_INTERNS_LIST, MOCK_REQUESTS_LIST } from '@core/mocks';
import { cutList } from '@shared/utils';


enum HrAction {
  VIEW_MY_REQUESTS = 'VIEW_MY_REQUESTS',
  SEND_MENTOR_REQUEST = 'SEND_MENTOR_REQUEST',
  SEND_CURATOR_REQUEST = 'SEND_CURATOR_REQUEST',
}

const HR_ACTIONS: ListOption<HrAction>[] = [
  {
    code: HrAction.VIEW_MY_REQUESTS,
    title: 'Посмотреть мои запросы',
  },
  {
    code: HrAction.SEND_MENTOR_REQUEST,
    title: 'Отправить запрос наставнику',
  },
  {
    code: HrAction.SEND_CURATOR_REQUEST,
    title: 'Отправить запрос куратору',
  },
];

@Component({
  selector: 'app-hr',
  templateUrl: './hr.component.html',
  styleUrls: ['./hr.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HrComponent {
  readonly NameFormat = NameFormat;
  readonly hrActions: ListOption<HrAction>[] = HR_ACTIONS;

  public candidates: BaseUserModel[] = MOCK_CANDIDATES_LIST;
  public approvedCandidates: BaseUserModel[] = cutList<BaseUserModel>(MOCK_CANDIDATES_LIST);

  public interns: BaseUserModel[] = MOCK_INTERNS_LIST;
  public organization: OrganizationModel = MOCK_REQUESTS_LIST[0].organization;
}
