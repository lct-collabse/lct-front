import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ListOption } from '@core/interfaces';



enum ToMotivationAction {
  EDIT = 'EDIT',
}

enum ToProfileAction {
  EDIT = 'EDIT',
}

const TO_MOTIVATION_ACTIONS: ListOption<ToMotivationAction>[] = [
  {
    code: ToMotivationAction.EDIT,
    title: 'Заполнить анкету',
  },
];


const TO_PROFILE_ACTIONS: ListOption<ToProfileAction>[] = [
  {
    code: ToProfileAction.EDIT,
    title: 'Заполнить профиль',
  },
];

const MOTIVATION_FORM_URL: string = 'https://forms.yandex.ru/u/64728dac02848f3e1caa0732';
const PROFILE_FORM_URL: string = 'https://forms.yandex.ru/u/6484a522068ff015716535b0';


@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CandidateComponent {
  readonly toMotivationActions: ListOption<ToMotivationAction>[] = TO_MOTIVATION_ACTIONS;
  readonly toProfileActions: ListOption<ToProfileAction>[] = TO_PROFILE_ACTIONS;

  public handleMotivationFormAction(code: ToMotivationAction): void {
    if (code === ToMotivationAction.EDIT) {
      window.open(MOTIVATION_FORM_URL, "_blank");
    }
  }

  public handleProfileFormAction(code: ToProfileAction): void {
    if (code === ToProfileAction.EDIT) {
      window.open(PROFILE_FORM_URL, "_blank");
    }
  }
}
