import { ChangeDetectionStrategy, Component, Inject, OnInit, Self } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TuiDestroyService } from '@taiga-ui/cdk';

import { STUDENT_POSITIONS } from '@core/consts';
import { AppMode, StudentPosition, UserRole } from '@core/enums';
import { ListOption } from '@core/interfaces';
import { StateService } from '@core/services';
import { takeUntil } from 'rxjs';


@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TuiDestroyService],
})
export class StudentComponent implements OnInit {
  public positions: ListOption<StudentPosition>[] = STUDENT_POSITIONS;

  public positionControl = new FormControl(STUDENT_POSITIONS[0]);

  public appMode: AppMode | null = null;

  constructor(
    private readonly state: StateService,

    @Self()
    @Inject(TuiDestroyService)
    private readonly destroy$: TuiDestroyService,
  ) {}

  ngOnInit(): void {
    this.appMode = this.state.appMode$.getValue()!;
    this.setupPositionHandling();
  }

  private setupPositionHandling(): void {
    if (this.appMode === AppMode.DEMO) {
      const demoStudentPositionCode: StudentPosition = this.state.demoStudentPosition$.getValue()!;

      const demoStudentPosition: ListOption<StudentPosition> = STUDENT_POSITIONS
        .find((item: ListOption<StudentPosition>) => item.code === demoStudentPositionCode)!;

      this.positionControl.setValue(demoStudentPosition);

      this.positionControl.valueChanges
        .pipe(takeUntil(this.destroy$))
        .subscribe((position: any) => {
          const positionCode: StudentPosition = (position! as ListOption<StudentPosition>).code;
          this.state.changeDemoUserRole(UserRole.STUDENT, positionCode);
        });

      return;
    }
  }
}
