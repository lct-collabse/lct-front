import { ChangeDetectionStrategy, Component } from '@angular/core';

import { NameFormat } from '@core/enums';
import { ListOption } from '@core/interfaces';
import { BaseUserModel, OrganizationModel } from '@core/models';

import { MOCK_INTERNS_LIST, MOCK_REQUESTS_LIST } from '@core/mocks';


enum ToProfileAction {
  VIEW = 'VIEW',
}

enum ToTabelAction {
  DOWNLOAD = 'DOWNLOAD',
}

enum InternAction {
  SEND_MENTOR_REQUEST = 'SEND_MENTOR_REQUEST',
  SEND_CURATOR_REQUEST = 'SEND_CURATOR_REQUEST',
  GIVE_MENTOR_FEEDBACK = 'GIVE_MENTOR_FEEDBACK',
}

const TO_PROFILE_ACTIONS: ListOption<ToProfileAction>[] = [
  {
    code: ToProfileAction.VIEW,
    title: 'Посмотреть профиль',
  },
];

const TO_TABEL_ACTIONS: ListOption<ToTabelAction>[] = [
  {
    code: ToTabelAction.DOWNLOAD,
    title: 'Скачать',
  },
];

const INTERN_ACTIONS: ListOption<InternAction>[] = [
  {
    code: InternAction.SEND_MENTOR_REQUEST,
    title: 'Отправить запрос наставнику',
  },
  {
    code: InternAction.SEND_CURATOR_REQUEST,
    title: 'Отправить запрос куратору',
  },
  {
    code: InternAction.GIVE_MENTOR_FEEDBACK,
    title: 'Дать обратную связь наставнику',
  },
];

@Component({
  selector: 'app-intern',
  templateUrl: './intern.component.html',
  styleUrls: ['./intern.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InternComponent {
  readonly NameFormat = NameFormat;
  readonly toProfileActions: ListOption<ToProfileAction>[] = TO_PROFILE_ACTIONS;
  readonly toTabelActions: ListOption<ToTabelAction>[] = TO_TABEL_ACTIONS;

  readonly intern: BaseUserModel = MOCK_INTERNS_LIST[0];
  readonly director: BaseUserModel = MOCK_REQUESTS_LIST[0].director;
  readonly organization: OrganizationModel = MOCK_REQUESTS_LIST[0].organization;
  readonly internActions: ListOption<InternAction>[] = INTERN_ACTIONS;

  public interns: BaseUserModel[] = MOCK_INTERNS_LIST;
}
