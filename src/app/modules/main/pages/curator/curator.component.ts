import { ChangeDetectionStrategy, Component } from '@angular/core';

import { BaseUserModel, RequestModel, TabelModel } from '@core/models';
import { ListOption } from '@core/interfaces';
import { NameFormat } from '@core/enums';

import {
  MOCK_CANDIDATES_LIST,
  MOCK_MENTORS_LIST,
  MOCK_REQUESTS_LIST,
  MOCK_TABELS_LIST,
} from '@core/mocks';


enum CuratorAction {
  SEND_MENTOR_REQUEST = 'SEND_MENTOR_REQUEST',
  VIEW_MY_REQUESTS = 'VIEW_MY_REQUESTS',
  MAKE_MASS_MAILING = 'MAKE_MASS_MAILING',
}

const CURATOR_ACTIONS: ListOption<CuratorAction>[] = [
  {
    code: CuratorAction.SEND_MENTOR_REQUEST,
    title: 'Запрос наставнику',
  },
  {
    code: CuratorAction.VIEW_MY_REQUESTS,
    title: 'Посмотреть мои запросы',
  },
  {
    code: CuratorAction.MAKE_MASS_MAILING,
    title: 'Сделать рассылку',
  },
];

@Component({
  selector: 'app-curator',
  templateUrl: './curator.component.html',
  styleUrls: ['./curator.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CuratorComponent {
  readonly NameFormat = NameFormat;
  readonly curatorActions: ListOption<CuratorAction>[] = CURATOR_ACTIONS;

  public candidates: BaseUserModel[] = MOCK_CANDIDATES_LIST;
  public mentors: BaseUserModel[] = MOCK_MENTORS_LIST;

  public requests: RequestModel[] = MOCK_REQUESTS_LIST;
  public tabels: TabelModel[] = MOCK_TABELS_LIST;
}
