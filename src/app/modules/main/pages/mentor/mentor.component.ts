import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NameFormat } from '@core/enums';

import { ListOption } from '@core/interfaces';
import { BaseUserModel } from '@core/models';

import { MOCK_CANDIDATES_LIST, MOCK_INTERNS_LIST } from '@core/mocks';


enum ToCandidateAction {
  VIEW_PROFILE = 'VIEW_PROFILE',
}

enum ToTabelAction {
  UPLOAD = 'UPLOAD',
}

enum MentorAction {
  GIVE_INTERNSHIP_FEEDBACK = 'GIVE_INTERNSHIP_FEEDBACK',
  SEND_CURATOR_REQUEST = 'SEND_CURATOR_REQUEST',
  VIEW_MY_REQUESTS = 'VIEW_MY_REQUESTS',
}

const TO_CANDIDATE_ACTIONS: ListOption<ToCandidateAction>[] = [
  {
    code: ToCandidateAction.VIEW_PROFILE,
    title: 'Посмотреть профиль',
  },
];

const TO_TABEL_ACTIONS: ListOption<ToTabelAction>[] = [
  {
    code: ToTabelAction.UPLOAD,
    title: 'Загрузить',
  },
];

const MENTOR_ACTIONS: ListOption<MentorAction>[] = [
  {
    code: MentorAction.GIVE_INTERNSHIP_FEEDBACK,
    title: 'Дать обратную связь по стажировке',
  },
  {
    code: MentorAction.SEND_CURATOR_REQUEST,
    title: 'Отправить запрос куратору',
  },
  {
    code: MentorAction.VIEW_MY_REQUESTS,
    title: 'Мои запросы',
  },
];

@Component({
  selector: 'app-mentor',
  templateUrl: './mentor.component.html',
  styleUrls: ['./mentor.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MentorComponent {
  readonly NameFormat = NameFormat;
  readonly toCandidateActions: ListOption<ToCandidateAction>[] = TO_CANDIDATE_ACTIONS;
  readonly toTabelActions: ListOption<ToTabelAction>[] = TO_TABEL_ACTIONS;
  readonly mentorActions: ListOption<MentorAction>[] = MENTOR_ACTIONS;

  public candidates: BaseUserModel[] = MOCK_CANDIDATES_LIST;
  public interns: BaseUserModel[] = MOCK_INTERNS_LIST;
}
