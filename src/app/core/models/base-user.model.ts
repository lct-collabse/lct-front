export interface BaseUserModel {
  id: number;
  firstName: string;
  lastName: string;
  middleName: string;
}
