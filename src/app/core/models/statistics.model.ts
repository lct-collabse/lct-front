export interface StatisticsModel {
  label: string;
  value: number;
}
