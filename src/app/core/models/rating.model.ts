export interface RatingModel<T> {
  position: number;
  entity: T;
}
