import { BaseUserModel } from "./base-user.model";
import { OrganizationModel } from "./organization.model";


export interface RequestModel {
  id: number;
  organization: OrganizationModel;
  director: BaseUserModel;
}
