export interface OrganizationModel {
  id: number;
  fullName: string;
  shortName: string;
}
