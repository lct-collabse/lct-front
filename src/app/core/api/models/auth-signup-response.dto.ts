import { UserRole } from "@core/enums";

export interface AuthSignupResponseDto {
  email: string;
  id: number;
  password: string;
  role: UserRole;
}
