import { UserRole } from "@core/enums";

export interface AuthUserDataDto {
  id: number;
  email: string;
  role: UserRole;
}
