import { AuthUserDataDto } from "./auth-user-data.dto";

export interface AuthLoginResponseDto {
  token: string;
  token_type: string;
  user: AuthUserDataDto;
}
