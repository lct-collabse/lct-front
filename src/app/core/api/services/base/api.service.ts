import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// TODO: uncomment for classic multi-env deploy
// import { environment } from "@env/environment";


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // TODO: uncomment for classic multi-env deploy
  // private host = environment.apiUrl;
  private host = '/api/v0';

  constructor(private http: HttpClient) {}

  public get(route: string, params?: any): Observable<any> {
    return this.http.get(`${this.host}/${route}`, params);
  }

  public post(route: string, data: any, params?: any): Observable<any> {
    return this.http.post(`${this.host}/${route}`, data, params);
  }

  public put(route: string, data: any, params?: any): Observable<any> {
    return this.http.put(`${this.host}/${route}`, data, params);
  }

  public delete(route: string, params?: any): Observable<any> {
    return this.http.delete(`${this.host}/${route}`, params);
  }
}
