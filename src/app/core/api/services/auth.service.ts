import { Injectable } from "@angular/core";
import { ApiService } from "./base/api.service";
import { Observable, map } from "rxjs";
import { UserRole } from "@core/enums";
import { AuthLoginResponseDto, AuthSignupResponseDto } from "../models";


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private readonly apiService: ApiService) {}

  public login(login: string, password: string): Observable<AuthLoginResponseDto> {
    return this.apiService.post('login', {
      email: login,
      password: password,
    }).pipe(
      map(({data}) => data),
    );
  }

  public signup(role: UserRole, login: string, password: string): Observable<AuthSignupResponseDto> {
    return this.apiService.post('signup', {
      role: role,
      email: login,
      password: password,
    }).pipe(
      map(({data}) => data),
    );
  }
}
