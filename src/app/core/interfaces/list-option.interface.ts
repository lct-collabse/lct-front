export interface ListOption<T = string> {
  code: T;
  title: string;
}
