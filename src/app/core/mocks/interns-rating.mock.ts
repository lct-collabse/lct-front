import { BaseUserModel, RatingModel } from "../models";


export const MOCK_INTERNS_RATING: RatingModel<BaseUserModel>[] = [
  {
    position: 40,
    entity: { id: 1,  lastName: 'Аксёнов',  firstName: 'Леонид',  middleName: 'Георгиевич' },
  },
  {
    position: 41,
    entity: { id: 3,  lastName: 'Ермаков',  firstName: 'Родион',  middleName: 'Владленович' },
  },
  {
    position: 42,
    entity: { id: 5,  lastName: 'Лазарев',  firstName: 'Даниил',  middleName: 'Аркадьевич' },
  },
];
