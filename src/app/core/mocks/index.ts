export { MOCK_MENTORS_LIST } from './mentors-list.mock';
export { MOCK_CANDIDATES_LIST } from './candidates-list.mock';

export { MOCK_INTERNS_LIST } from './interns-list.mock';
export { MOCK_INTERNS_RATING } from './interns-rating.mock';

export { MOCK_REQUESTS_LIST } from './requests-list.mock';
export { MOCK_TABELS_LIST } from './tabels-list.mock';
