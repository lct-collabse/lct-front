import { BaseUserModel } from "../models";


export const MOCK_CANDIDATES_LIST: BaseUserModel[] = [
  { id: 1,  lastName: 'Колобов',   firstName: 'Александр',  middleName: 'Сергеевич' },
  { id: 2,  lastName: 'Смирнова',  firstName: 'Анна',       middleName: 'Ивановна' },
  { id: 3,  lastName: 'Морозов',   firstName: 'Дмитрий',    middleName: 'Алексеевич' },
  { id: 4,  lastName: 'Петрова',   firstName: 'Светлана',   middleName: 'Михайловна' },
  { id: 5,  lastName: 'Иванов',    firstName: 'Иван',       middleName: 'Анатольевич' },
  { id: 6,  lastName: 'Волкова',   firstName: 'Елена',      middleName: 'Викторовна' },
  { id: 7,  lastName: 'Кузнецов',  firstName: 'Максим',     middleName: 'Валерьевич' },
];
