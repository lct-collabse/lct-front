import { BaseUserModel } from "../models";


export const MOCK_INTERNS_LIST: BaseUserModel[] = [
  { id: 1,  lastName: 'Аксёнов',      firstName: 'Леонид',    middleName: 'Георгиевич' },
  { id: 2,  lastName: 'Виноградова',  firstName: 'Зоя',       middleName: 'Филипповна' },
  { id: 3,  lastName: 'Ермаков',      firstName: 'Родион',    middleName: 'Владленович' },
  { id: 4,  lastName: 'Боброва',      firstName: 'Валерия',   middleName: 'Виталиевна' },
  { id: 5,  lastName: 'Лазарев',      firstName: 'Даниил',    middleName: 'Аркадьевич' },
  { id: 6,  lastName: 'Дементьева',   firstName: 'София',     middleName: 'Святославовна' },
  { id: 7,  lastName: 'Горбунов',     firstName: 'Никита',    middleName: 'Анатольевич' },
];
