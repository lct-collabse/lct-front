import { BaseUserModel } from "../models";


export const MOCK_MENTORS_LIST: BaseUserModel[] = [
  { id: 1,  lastName: 'Волков',   firstName: 'Борис',       middleName: 'Геннадьевич' },
  { id: 2,  lastName: 'Павлова',  firstName: 'Людмила',     middleName: 'Викторовна' },
  { id: 3,  lastName: 'Зайцев',   firstName: 'Алексей',     middleName: 'Андреевич' },
  { id: 4,  lastName: 'Фролова',  firstName: 'Мария',       middleName: 'Ивановна' },
  { id: 5,  lastName: 'Лебедев',  firstName: 'Константин',  middleName: 'Игоревич' },
  { id: 6,  lastName: 'Баранова', firstName: 'Анна',        middleName: 'Сергеевна' },
  { id: 7,  lastName: 'Королев',  firstName: 'Денис',       middleName: 'Юрьевич' },
];
