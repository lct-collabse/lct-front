import { TabelModel } from "../models";

export const MOCK_TABELS_LIST: TabelModel[] = [
  { id: 1, name: 'Табель 1', created: '01.02.2023', url: 'http://tabel-1' },
  { id: 2, name: 'Табель 2', created: '02.03.2023', url: 'http://tabel-2' },
  { id: 3, name: 'Табель 3', created: '03.04.2023', url: 'http://tabel-3' },
  { id: 4, name: 'Табель 4', created: '04.05.2023', url: 'http://tabel-4' },
  { id: 5, name: 'Табель 5', created: '05.06.2023', url: 'http://tabel-5' },
  { id: 6, name: 'Табель 6', created: '06.07.2023', url: 'http://tabel-6' },
  { id: 7, name: 'Табель 7', created: '07.08.2023', url: 'http://tabel-7' },
];
