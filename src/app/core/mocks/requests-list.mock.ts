import { RequestModel } from "../models";

// http://government.ru/ministries
export const MOCK_REQUESTS_LIST: RequestModel[] = [
  {
    id: 1,
    organization: { id: 1, fullName: 'Министерство здравоохранения', shortName: 'Минздрав' },
    director: { id: 1, lastName: 'Белов', firstName: 'Владимир', middleName: 'Николаевич' },
  },
  {
    id: 2,
    organization: { id: 2, fullName: 'Министерство культуры', shortName: 'Минкультуры' },
    director: { id: 2, lastName: 'Васильева', firstName: 'Ирина', middleName: 'Александровна' },
  },
  {
    id: 3,
    organization: { id: 3, fullName: 'Министерство сельского хозяйства', shortName: 'Минсельхоз' },
    director: { id: 3, lastName: 'Соколов', firstName: 'Николай', middleName: 'Евгеньевич' },
  },
  {
    id: 4,
    organization: { id: 4, fullName: 'Министерство спорта', shortName: 'Минспорт' },
    director: { id: 4, lastName: 'Попова', firstName: 'Татьяна', middleName: 'Сергеевна' },
  },
  {
    id: 5,
    organization: { id: 5, fullName: 'Министерство транспорта', shortName: 'Минтранс' },
    director: { id: 6, lastName: 'Жуков', firstName: 'Андрей', middleName: 'Петрович' },
  },
  {
    id: 6,
    organization: { id: 6, fullName: 'Министерство финансов', shortName: 'Минфин' },
    director: { id: 6, lastName: 'Федорова', firstName: 'Ольга', middleName: 'Васильевна' },
  },
  {
    id: 7,
    organization: { id: 7, fullName: 'Министерство энергетики', shortName: 'Минэнерго' },
    director: { id: 7, lastName: 'Павлов', firstName: 'Игорь', middleName: 'Александрович' },
  },
];
