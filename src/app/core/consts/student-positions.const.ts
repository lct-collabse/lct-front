import { StudentPosition } from "@core/enums";
import { ListOption } from "@core/interfaces";


export const STUDENT_POSITIONS: ListOption<StudentPosition>[] = [
  { code: StudentPosition.CANDIDATE,  title: 'Кандидат' },
  { code: StudentPosition.INTERN,     title: 'Стажер' },
];
