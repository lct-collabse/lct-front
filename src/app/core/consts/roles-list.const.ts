import { UserRole } from "@core/enums";
import { ListOption } from "@core/interfaces";


export const ROLES_LIST: ListOption<UserRole>[] = [
  { code: UserRole.CURATOR, title: 'Куратор' },
  { code: UserRole.MENTOR,  title: 'Наставник' },
  { code: UserRole.HR,      title: 'Кадровик' },
  { code: UserRole.STUDENT, title: 'Студент' },
];
