import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

// MODELS
import { AuthUserDataDto } from '@core/api/models';

// ENUMS
import { AuthRouting } from '@auth/index';
import { AppMode, GlobalRouting, StorageVar, StudentPosition, UserRole } from '@core/enums';


const DEFAULT_USER_ROLE = UserRole.CURATOR;
const DEFAULT_STUDENT_POSITION = StudentPosition.CANDIDATE;


@Injectable({
  providedIn: 'root'
})
export class StateService {
  public isAuth$ = new BehaviorSubject<boolean>(false);
  public user$ = new BehaviorSubject<AuthUserDataDto | null>(null);

  public appMode$ = new BehaviorSubject<AppMode | null>(null);
  public demoUserRole$ = new BehaviorSubject<UserRole | null>(null);
  public demoStudentPosition$ = new BehaviorSubject<StudentPosition | null>(null);

  constructor(
    private readonly router: Router,
  ) {}

  public init(): void {
    const mode: AppMode | null = localStorage.getItem(StorageVar.APP_MODE) as AppMode;
    this.appMode$.next(mode);

    if (mode === AppMode.DEMO) {
      const demoUserRole: UserRole = localStorage
        .getItem(StorageVar.DEMO_ROLE) as UserRole || DEFAULT_USER_ROLE;

      const demoStudentPosition: StudentPosition = localStorage
        .getItem(StorageVar.DEMO_POSITION) as StudentPosition || DEFAULT_STUDENT_POSITION;

      this.changeDemoUserRole(demoUserRole, demoStudentPosition);

      return;
    }

    const sourceToken: string | null = localStorage.getItem(StorageVar.ACCESS_TOKEN);
    const sourceUser: string | null = localStorage.getItem(StorageVar.USER_DATA);

    if (!sourceToken || !sourceUser) {
      this.router.navigate([`/${GlobalRouting.AUTH}/${AuthRouting.LOGIN}`]);
      return;
    }

    const user: AuthUserDataDto = JSON.parse(sourceUser);

    this.isAuth$.next(true);
    this.user$.next(user);

    this.router.navigate([`/${user.role}`]);
  }

  public login(user: AuthUserDataDto, token: string): void {
    this.isAuth$.next(true);
    this.user$.next(user);

    localStorage.setItem(StorageVar.ACCESS_TOKEN, token);
    localStorage.setItem(StorageVar.USER_DATA, JSON.stringify(user));

    this.router.navigate([`/${user.role}`]);
  }

  public logout(): void {
    this.isAuth$.next(false);
    this.user$.next(null);

    localStorage.removeItem(StorageVar.ACCESS_TOKEN);
    localStorage.removeItem(StorageVar.USER_DATA);

    this.router.navigate([`/${GlobalRouting.AUTH}/${AuthRouting.LOGIN}`]);
  }

  public changeDemoUserRole(role: UserRole, position?: StudentPosition): void {
    const newRole: UserRole = role || DEFAULT_USER_ROLE;

    let newPath = `/${newRole}`
    this.demoUserRole$.next(newRole);
    localStorage.setItem(StorageVar.DEMO_ROLE, newRole);

    if (role === UserRole.STUDENT) {
      const newPosition: StudentPosition = position || DEFAULT_STUDENT_POSITION;

      newPath += `/${newPosition}`;
      this.demoStudentPosition$.next(newPosition);
      localStorage.setItem(StorageVar.DEMO_POSITION, newPosition);
    }

    this.router.navigate([newPath]);
  }
}
