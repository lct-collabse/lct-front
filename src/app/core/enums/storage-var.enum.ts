export enum StorageVar {
  ACCESS_TOKEN = 'access_token',
  USER_DATA = 'user_data',

  APP_MODE = 'app_mode',
  DEMO_ROLE = 'demo_role',
  DEMO_POSITION = 'demo_position',
}
