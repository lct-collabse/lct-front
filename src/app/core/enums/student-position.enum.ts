export enum StudentPosition {
  INTERN = 'intern',
  CANDIDATE = 'candidate',
}
