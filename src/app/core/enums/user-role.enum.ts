export enum UserRole {
  ADMIN = 'admin',
  STUDENT = 'student',
  CURATOR = 'curator',
  MENTOR = 'mentor',
  HR = 'hr'
}
