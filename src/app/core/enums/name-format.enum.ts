export enum NameFormat {
  CASUAL = 'casual',
  SHORT = 'short',
  LONG = 'long',
}
