const express = require('express');
const { createProxyMiddleware } = require('http-proxy-middleware');
const fs = require('fs');

const app = express();

// Директория 'dist' должна содержать результат сборки вашего Angular-приложения
app.use(express.static('./dist/app-front'));

// Считываем настройки прокси из файла proxy.conf.json
const proxySettings = JSON.parse(fs.readFileSync('proxy.conf.json'));

// Применяем каждую из настроек прокси
for (let path in proxySettings) {
  app.use(path, createProxyMiddleware(proxySettings[path]));
}

app.listen(80, () => {
  console.log('Server is running on port 80');
});
